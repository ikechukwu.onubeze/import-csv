# Description

A simple application for reading and processing a csv file.
Reads a csv file with possible extension to read other types of files, and processes it by converting each row of the csv file into a JSON document, applying some validation to ensure data integrity and also writing the document to the console. There is possibility to extend the application to include further validation and also to write processed JSON data to another destination like Database or external web service.

# Build dependencies
- [Java 8](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html)
- [Maven, 3.8.1](https://maven.apache.org/download.cgi)

# Test
`mvn test`

# Compile
`mvn clean compile assembly:single`

# Execute
`java -jar target/import-csv-0.1-jar-with-dependencies.jar -i <absolute_path_to_input_file>`

The input file can be placed in any accessible location in the local file system, possibly in the same directory as the src folder
