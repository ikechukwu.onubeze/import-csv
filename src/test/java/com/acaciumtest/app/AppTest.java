package com.acaciumtest.app;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.acaciumtest.app.handler.WriteToConsoleDataHandler;
import com.acaciumtest.app.io.CSVFileReader;
import com.acaciumtest.app.io.ConsoleJsonDataWriter;
import com.acaciumtest.app.validator.NonEmptyShiftTypeColumnValidator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.File;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    String path = "src/test/resources/input.csv";
    File file = new File(path);
    String absolutePath = file.getAbsolutePath();

    private final CSVFileReader fileReader = new CSVFileReader();
    private final ConsoleJsonDataWriter dataWriter = new ConsoleJsonDataWriter();
    private final NonEmptyShiftTypeColumnValidator validator = new NonEmptyShiftTypeColumnValidator();
    private final WriteToConsoleDataHandler handler = new WriteToConsoleDataHandler(fileReader, validator, dataWriter);

    @Test
    public void CSVFileReaderShouldFetchListOfJsonObjects() throws Exception{
        assertTrue( !fileReader.read(absolutePath).isEmpty());
    }

    @Test
    public void NonEmptyShiftTypeColumnValidatorShouldSucceed() throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        String json = "{\"creator\":\"ea2da28f-9dae-430c-be68-9c35f06e0ff1\",\"break\":\"60\",\"description\":\"Community Nurse Required - Early - W1 - 16:04\",\"start\":\"2018-03-20T07:00:00.000Z\",\"end\":\"2018-03-20T15:30:00.000Z\",\"shiftType\":\"early\"}\n";
        JsonNode jsonNode = objectMapper.readTree(json);
        assertTrue( validator.validate(jsonNode));
    }

    @Test
    public void NonEmptyShiftTypeColumnValidatorShouldFail() throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        String json = "{\"creator\":\"ea2da28f-9dae-430c-be68-9c35f06e0ff1\",\"break\":\"60\",\"description\":\"Community Nurse Required - Early - W1 - 16:04\",\"start\":\"2018-03-20T07:00:00.000Z\",\"end\":\"2018-03-20T15:30:00.000Z\",\"shiftType\":\"\"}\n";
        JsonNode jsonNode = objectMapper.readTree(json);
        assertFalse( validator.validate(jsonNode));
    }

    @Test
    public void ConsoleJsonDataWriterShouldExecuteSuccessfully() throws Exception{
        dataWriter.write(fileReader.read(absolutePath).get(0));
    }

    @Test
    public void WriteToConsoleDataHandlerShouldSucceed() throws Exception{
        handler.handle(absolutePath);
    }
}
