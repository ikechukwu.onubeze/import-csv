package com.acaciumtest.app.io;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
/**
 * Basic file reading operation
 * @author augustineonubeze
 *
 */
public interface FileReader {
	/**
	 * Read the content of a file as a list of String,
	 * each element representing a line in the file.
	 * @param filePath path to the file to be read
	 * @return content of the file as a list of JsonNodes
	 * @throws IOException
	 */
	public List<JsonNode> read(String filePath) throws IOException;

}
