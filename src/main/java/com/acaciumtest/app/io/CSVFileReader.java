package com.acaciumtest.app.io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

/**
 * CSV file reader. Reads the content of a CSV file as a list of JsonNodes
 * @author augustineonubeze
 *
 */
public class CSVFileReader implements FileReader {

	@Override
	public List<JsonNode> read(String filePath) throws IOException {
		File inputFile = new File(filePath);
		
		CsvSchema bootstrap = CsvSchema.emptySchema().withHeader();
        CsvMapper csvMapper = new CsvMapper();
        MappingIterator<Map<?, ?>> mappingIterator = csvMapper.readerFor(Map.class).with(bootstrap).readValues(inputFile);
        ObjectMapper mapper = new ObjectMapper();
        List<JsonNode> jsonNodes = new ArrayList<>();
        while(mappingIterator.hasNext()) {
        	jsonNodes.add(mapper.readTree(mapper.writeValueAsString(mappingIterator.next())));
        }
        
        return jsonNodes;
	}

}
