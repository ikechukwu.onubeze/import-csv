package com.acaciumtest.app.io;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * A class to write JSON object to console
 * @author augustineonubeze
 *
 */
public class ConsoleJsonDataWriter implements JsonDataWriter {

	@Override
	public void write(JsonNode jsonNode) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode));
	}

}
