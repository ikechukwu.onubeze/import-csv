package com.acaciumtest.app.io;

import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

/**
 * Simple operation to write JSON object to given destination
 * @author augustineonubeze
 *
 */
public interface JsonDataWriter {

	/**
	 * Writes JSON object to a given destination based on implementation
	 * @param jsonNode JSON object to be written
	 */
	public void write(JsonNode jsonNode) throws IOException;
}
