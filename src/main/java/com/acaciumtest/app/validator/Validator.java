package com.acaciumtest.app.validator;

/**
 * A basic validation operation
 * @author augustineonubeze
 *
 */
public interface Validator {

	/**
	 * Validates object obj based on specified rules
	 * @param obj object to be validated
	 * @return true or false
	 */
	public boolean validate(Object obj);
}
