package com.acaciumtest.app.validator;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * A simple Validator to ensure that only rows with non-empty value for 
 * shiftType column are selected
 * @author augustineonubeze
 *
 */
public class NonEmptyShiftTypeColumnValidator implements Validator{

	@Override
	public boolean validate(Object obj) {
		JsonNode jsonNode = JsonNode.class.cast(obj);
		JsonNode shiftType = jsonNode.get("shiftType");

		return !(shiftType == null || shiftType.asText().isEmpty());
	}

}
