package com.acaciumtest.app.handler;

import java.io.IOException;

/**
 * A simple data handling operation
 * @author augustineonubeze
 *
 */
public interface DataHandler {

	/**
	 * Handles data from the given source, typically file
	 * @param  path path to the data source
	 */
	public void handle(String path) throws IOException;
}
