package com.acaciumtest.app.handler;

import java.io.IOException;

import com.acaciumtest.app.io.FileReader;
import com.acaciumtest.app.io.JsonDataWriter;
import com.acaciumtest.app.validator.Validator;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Handles CSV data file by converting each row to a JSON document,
 * removing all rows without any value for the 'shiftType' column
 * and writing the JSON documents to the console.
 * 
 * @author augustineonubeze
 *
 */
public class WriteToConsoleDataHandler implements DataHandler{
	
	private FileReader fileReader;
	private Validator validator;
	private JsonDataWriter dataWriter;
	
	public WriteToConsoleDataHandler(FileReader fileReader, Validator validator, JsonDataWriter dataWriter) {
		this.fileReader = fileReader;
		this.validator = validator;
		this.dataWriter = dataWriter;
	}

	@Override
	public void handle(String path) throws IOException{
		fileReader.read(path).stream().map(d -> JsonNode.class.cast(d)) // Read file content as list of JsonNodes
		.filter(node -> validator.validate(node)) // Select only rows with non empty value for column 'shiftType'
    	.forEach(node -> {
			try {
				dataWriter.write(node);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}); // Print each row of JSON object to console
	}

}
