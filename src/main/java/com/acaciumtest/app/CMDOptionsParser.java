package com.acaciumtest.app;

import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class CMDOptionsParser {

	public static String parse(String [] args) {
		Options options = new Options();
    	Option inputOption = new Option("i", "input", true, "Absolute path to input file");
    	inputOption.setRequired(true);
    	options.addOption(inputOption);
    	
    	CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();

        try {
            return parser.parse(options, args).getOptionValue("input");
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("<executable> -i <path_to_input_file>", options);
            return null;
        }
	}
}
