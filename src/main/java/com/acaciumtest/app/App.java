package com.acaciumtest.app;


import com.acaciumtest.app.handler.WriteToConsoleDataHandler;
import com.acaciumtest.app.io.CSVFileReader;
import com.acaciumtest.app.io.ConsoleJsonDataWriter;
import com.acaciumtest.app.io.JsonDataWriter;
import com.acaciumtest.app.validator.NonEmptyShiftTypeColumnValidator;
import com.acaciumtest.app.validator.Validator;

/**
 * A simple application to read CSV file from specified path,
 * convert each row to a JSON document, remove all rows with
 * empty value for the column 'shiftType', and print each row
 * to the console.
 * 
 * Implemented as a requirement in the application process for the 
 * Senior Java Developer position in Acacium Group.
 * 
 * @author augustineonubeze
 *
 */
public class App {

    public static void main(String[] args) throws Exception{
    	// Fetch the input file path from the command line options
    	String inputFilePath = CMDOptionsParser.parse(args);
    	if(inputFilePath == null) {
    		// Input file path is required, quit application with non zero code
    		// if input path is not provided.
    		System.exit(1);
    	}
    	CSVFileReader fileReader = new CSVFileReader();
    	Validator validator = new NonEmptyShiftTypeColumnValidator();
    	JsonDataWriter dataWriter = new ConsoleJsonDataWriter();
    	WriteToConsoleDataHandler handler = new WriteToConsoleDataHandler(fileReader, validator, dataWriter);
    	handler.handle(inputFilePath);
    }
}
